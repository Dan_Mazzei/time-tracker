#!/usr/bin/python3

import time
import json
import sys
import datetime

#this is a script to track time spent on various tasks

#it works by keeping a json file of time spent
#the json file is organized in the following way:
'''
[
	{
		"name":"Project",
		"tasks":[{
			"name":"Task",
			"sessions":[
				{
				"start":start_timestamp,
				"end":end_timestamp
				}
			]
		}]
	}
]
'''

#task time is gathered by summing all session times for the task

#time is logged by adding a new session, and either adding to an existing task or a new task
#when a new task is made, is is added to either an existing project or a new project (last logged project by default)


#format a given text string to be centered in a full-width string
#for headings and such
def full_line_frmt(string,filler_char,padding_char,columns):
	#never cut off the string itself
	if(columns<len(string)):
		return string
	
	#the string should be centered, so it should start at
	#(columns/2)-(len(string)/2) = (columns-len(string))/2
	#minus a character of padding
	string_start_col=(int)(((columns-len(string))/2)-len(padding_char))
	
	acc=''
	for n in range(0,columns):
		if(n<string_start_col):
			if(n==(string_start_col-1)):
				acc+=padding_char
			else:
				acc+=filler_char
		elif((n>=string_start_col) and (n<string_start_col+len(string))):
			acc+=string[n-string_start_col]
		elif(n==(string_start_col+len(string))):
			acc+=padding_char
		else:
			acc+=filler_char
	
	return acc

def pretty_json(json_obj):
	return json.dumps(json_obj, sort_keys=True, indent=4, separators=(',', ': '))

def pad_str_to(string,length,pad_char):
	while(len(string)<length):
		string+=pad_char
	return string
	

#run a session and return the associated timestamps
def run_session():
	print('Timing new work session; press enter to stop...')
	start_time=time.time()
	print('Session started on '+datetime.datetime.fromtimestamp(start_time).strftime('%Y-%m-%d %H:%M:%S'))
	
	#we don't actually care about the content, so we just read a line and throw it out
	#this effectively pauses for an enter
	input()
	end_time=time.time()
	print('Session ended on '+datetime.datetime.fromtimestamp(end_time).strftime('%Y-%m-%d %H:%M:%S'))
	
	#we now have start and end timestamps for the session, so return a json-encodable object with that information
	session={}
	session['start']=start_time
	session['end']=end_time
	return session

#log a session in a given project and task to a file
def log_session(filename,project,task,session):
	#always put the date with the task, because even though we want multiple sessions per task
	#we still want to clearly determine what was done on a given day
	task=datetime.datetime.now().strftime('%Y-%m-%d')+' '+task
	try:
		fp=open(filename,'r')
		fcontent=fp.read()
		fp.close()
		
		project_times=json.loads(fcontent)
	except Exception:
		project_times=[]
	
	#find the appropriate spot to put the session in the existing structure
	#if an appropriate project and task does not yet exist, create it now
	for proj_obj in project_times:
		if(proj_obj['name']==project):
			for task_obj in proj_obj['tasks']:
				if(task_obj['name']==task):
					task_obj['sessions'].append(session)
					break
			else:
				print('Warn: Did not find pre-existing task '+task+'; adding now...')
				task_obj={}
				task_obj['name']=task
				task_obj['sessions']=[session]
				proj_obj['tasks'].append(task_obj)
			break
	else:
		print('Warn: Did not find pre-existing project '+project+'; adding now...')
		task_obj={}
		task_obj['name']=task
		task_obj['sessions']=[session]
		proj_obj={}
		proj_obj['name']=project
		proj_obj['tasks']=[task_obj]
		project_times.append(proj_obj)

	
	#write the updated file, now that this session has been logged in the json
	fp=open(filename,'w')
	fp.write(pretty_json(project_times))
	fp.close()

#show a summary of the given project's logged time from the given log file
def show_summary(filename,project):
	try:
		fp=open(filename,'r')
		fcontent=fp.read()
		fp.close()
		
		project_times=json.loads(fcontent)
	except Exception:
		project_times=[]
	
	#find the appropriate project based on the project name given
	for proj_obj in project_times:
		if(proj_obj['name']==project):
			project_time=0
			day_times={}
			print(full_line_frmt(project,'=',' ',80))
			
			for task_obj in proj_obj['tasks']:
				print('=== Task: '+task_obj['name'])
				min_start_time=time.time()
				task_time=0
				for session_obj in task_obj['sessions']:
					if(session_obj['start']<min_start_time):
						min_start_time=session_obj['start']
					session_time=session_obj['end']-session_obj['start']
					task_time+=session_time
					#the session was done on the day that it started
					start_day=datetime.datetime.fromtimestamp(session_obj['start']).strftime('%Y-%m-%d')
					if(start_day in day_times):
						day_times[start_day]+=session_time
					else:
						day_times[start_day]=session_time
				
				min_start_str=datetime.datetime.fromtimestamp(min_start_time).strftime('%Y-%m-%d %H:%M:%S')
				task_time_str=str(task_time/60)+' minutes'
				print('===== Started on '+min_start_str)
				print('===== Time Spent was '+task_time_str)
				print('')
				
				project_time+=task_time
			break
	else:
		print('Err: Could not find project '+project+' in the log file; cannot show summary; sorry!')
		return
	#show total time per day
	print(full_line_frmt(project,'=',' ',80))
	day_times_list=[]
	for day in day_times:
		day_times_list.append((day,day_times[day]))
	day_times_list.sort(key=lambda x:x[0])
	for day in day_times_list:
		print('=== Time spent on '+day[0]+' was '+str(day[1]/60/60)+' hours')
	print(full_line_frmt('Total Time '+str((project_time/60)/60)+' hours','=',' ',80))

def list_projects(filename):
	try:
		fp=open(filename,'r')
		fcontent=fp.read()
		fp.close()
		
		project_times=json.loads(fcontent)
	except Exception:
		project_times=[]
	
	print(full_line_frmt('All Projects','=',' ',80))
	for proj_obj in project_times:
		project_time=0
		last_edit_time=0
		for task_obj in proj_obj['tasks']:
			task_time=0
			for session_obj in task_obj['sessions']:
				if(session_obj['end']>last_edit_time):
					last_edit_time=session_obj['end']
				session_time=session_obj['end']-session_obj['start']
				task_time+=session_time
			
			project_time+=task_time
		
		output_str=proj_obj['name']
		
		output_str=pad_str_to(output_str,30,' ')
		if(not output_str.endswith(' ')):
			output_str+=','
		
		output_str+=('%.1f' % ((project_time/60)/60))+' hours'
		output_str=pad_str_to(output_str,50,' ')
		if(not output_str.endswith(' ')):
			output_str+=','
		
		output_str+='last edit '+datetime.datetime.fromtimestamp(last_edit_time).strftime('%Y-%m-%d')

		
		print(output_str)
	print(full_line_frmt('End Project List','=',' ',80))
	
	
def print_usage():
	print('Usage: '+sys.argv[0]+' log <project> <task>')
	print('Usage: '+sys.argv[0]+' view <project>')
	print('Usage: '+sys.argv[0]+' list-projects')

if(__name__=='__main__'):
	if(len(sys.argv)<2):
		print_usage()
	elif(sys.argv[1]=='list-projects'):
		list_projects('time_logging.json')
	else:
		if(len(sys.argv)<3):
			print_usage()
		elif(sys.argv[1]=='view'):
			show_summary('time_logging.json',sys.argv[2])
		elif((sys.argv[1]=='log') and (len(sys.argv)>3)):
			session=run_session()
			log_session('time_logging.json',sys.argv[2],sys.argv[3],session)
		else:
			print_usage()


