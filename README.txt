This is a simple time tracking program because briefcase doesn't fit well into my work flow
It has the following properties:
	It can log time for multiple projects in a single file
	It can log time for multiple tasks within a project
	It can log time for multiple sessions within a task
		This means you can start a task, stop it (by ending the session), and later resume it (by creating a new session)
		Sessions are aggregated in the summary into a single "time spent" value per task which can be entered into another logging system
	It is designed to be unobtrusive, and as minimal as possible
		The entire interface consists of a bit of short command line data and hitting the "Enter" key
	

Uses:
	Usage: ./time_tracker.py log <project> <task>
		This starts a session for the given project and task (and adds it to the logging file if it wasn't previously there)
			Note that tasks are automatically prepended with a date, so that tasks are always date-specific

	Usage: ./time_tracker.py view <project>
		This lists all the tasks (with total time) for the given project
		A total time per day, and a total time for the project are also printed

	Usage: ./time_tracker.py list-projects
		This lists all the projects that are logged in the local json file, with total time spent and last update time also included


